my_name = "Adrian"
my_age = 34
my_email = "adrian.gonciarz@gmail.com"

print(my_name)
print(my_age)
print(my_email)

# Below is the description of my friend
friend_name = "Henryk"
friend_age = 44
friend_number_of_pets = 3
friend_has_driving_license = True
friendship_time_in_years = 2.5

print(friend_name, friend_age, friend_number_of_pets, friend_has_driving_license, friendship_time_in_years)
print("Friend's name:", friend_name, "\nFriend's age:", friend_age)
