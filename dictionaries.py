# friend = {
#     'name': 'John',
#     'age': 40,
#     'hobbies': ['swimming', 'cycling']
# }
#
# print(friend['age'])
# friend['city'] = 'Wrocław'
# friend['age'] = 39
# print(friend)
# friend['job'] = 'doctor'
# print(friend)
# del friend['job']
# print(friend)
#
# print(friend['hobbies'][-1])
# friend['hobbies'].append('climbing')
# print(friend)


def get_employee_info(email):
    return {
        'employee_email': email,
        'company': '4testers.pl'
    }


if __name__ == '__main__':
    info_1 = get_employee_info('robert@4testers.pl')
    info_2 = get_employee_info('magda@4testers.pl')
    info_3 = get_employee_info('zosia@4testers.pl')

    print(info_1)
    print(info_2)
    print(info_3)
