movies = ["Mission Impossible", "Niezniszczalni", "IronMan", "Transporter 2", "Szybcy i Wściekli"]

first_movie = movies[0]
print(f'First movie is {first_movie}')

print(f'Last movie is {movies[-1]}')

movies.append("Dune")
print(f'The length of movies list is {len(movies)}')

print(f'The length of word supercalifragilistic is {len("supercalifragilistic")}')

movies.insert(0, "Star Wars")
movies.remove("Niezniszczalni")
print(movies)

movies[-1] = "Dune (1980)"
print(movies)

# Exercise 1
emails = ['a@example.com', 'b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])

emails.append('cde@example.com')
